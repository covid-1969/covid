package models

import (
	"log"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var err error

// Geofence struct for geofence collection
type Geofence struct {
	ID string `json:"_id" bson:"_id"`
}

// Location struct for a GeoJson
type Location struct {
	Type        string    `json:"type" bson:"type"`
	Coordinates []float64 `json:"coordinates" bson:"coordinates"`
}

// NewPoint return a new point in GeoJSON format
func NewPoint(lng, lat float64) Location {
	return Location{
		"Point",
		[]float64{lng, lat},
	}
}

// CheckInside return true or false if user is inside of the assigned geofence
func CheckInside(lat float64, lng float64, ID int) (inside bool) {
	var geo Geofence
	inside = false
	geofences := db.Collection("geofences")
	filter := bson.D{
		{"user_id", ID},
		{"loc",
			bson.D{
				{"$geoIntersects", bson.D{
					{"$geometry", NewPoint(lng, lat)},
				}},
			}},
	}
	if err = geofences.FindOne(ctx, filter).Decode(&geo); err != nil {
		return
	}
	inside = true
	return
}

// AddGeofence save a new geofence
func AddGeofence(ID int, loc [][][]float64) (iden string) {
	coll := db.Collection("geofences")
	options := options.Replace()

	options.SetUpsert(true)
	doc, err := coll.ReplaceOne(ctx,
		bson.M{"user_id": ID},
		bson.D{
			{"user_id", ID},
			{"created_at", time.Now()},
			{"loc", bson.D{
				{"type", "Polygon"},
				{"coordinates", loc},
			}},
		},
		options,
	)
	if err != nil {
		log.Fatal(err)
		return
	}

	if oid, ok := doc.UpsertedID.(primitive.ObjectID); ok {
		iden = oid.Hex()
	}

	if doc.MatchedCount > 0 {
		iden = "updated"
	}
	return
}
