package v1

import (
	"covid/pkg/util/cache"
	"fmt"
	"math/rand"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
)

// GetAllPositions returns all users actual positions
// @Summary returns all users actual positions
// @Description This endpoint will return an array with last users positions
// @Accept  json
// @Produce  json
// @Router /positions/all [get]
// @Security Bearer
// @Tags Geo
func GetAllPositions(c *gin.Context) {
	keys := cache.Keys("users:*")
	var data [][]string
	for _, k := range keys {
		val := cache.HGetvalue(k)

		point := []string{val["lat"], val["lng"]}
		data = append(data, point)
	}
	if len(data) > 0 {
		c.JSON(http.StatusOK, gin.H{
			"success": true,
			"data":    data,
		})
	} else {
		c.JSON(http.StatusOK, gin.H{
			"success": true,
			"data":    []string{},
		})
	}
}

// CreateFakePositions create a dummy data for fill redis
func CreateFakePositions(c *gin.Context) {
	var n int = 5000
	latitudes := randFloats(-18.352222, -0.03, n)
	longitudes := randFloats(-81.326389, -68.6575, n)

	for i := 0; i < n; i++ {
		data := make(map[string]interface{})
		key := fmt.Sprintf("users:%d", i)
		data["lat"] = latitudes[i]
		data["lng"] = longitudes[i]
		go cache.HSetvalue(key, data)
	}

	c.JSON(http.StatusOK, gin.H{
		"success": true,
		"msg":     "All positions created!!!",
	})
}

// randFloats create an array of randoms float numbers
func randFloats(min, max float64, n int) []float64 {
	rand.Seed(time.Now().UnixNano())
	res := make([]float64, n)
	for i := range res {
		res[i] = min + rand.Float64()*(max-min)
	}
	return res
}
