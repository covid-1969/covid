package v1

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"regexp"
	"strconv"

	"github.com/astaxie/beego/validation"
	"github.com/gin-gonic/gin"
	"github.com/unknwon/com"

	"covid/models"
	"covid/pkg/util/cache"
)

// CreateGeofences Function to create a new geofence
// @Summary Function to create a new geofence
// @Description Allow to create a geofence and set to an user
// @Accept  json
// @Produce  json
// @Router /geofences [post]
// @Security Bearer
// @Tags Geo
func CreateGeofences(c *gin.Context) {
	type Body struct {
		Address string        `json:"address"`
		Loc     [][][]float64 `json:"loc"`
		Point   []float64     `json:"point"`
		Radius  int           `json:"radius"`
		Type    string        `json:"type"`
		UserID  int           `json:"user_id"`
	}

	var body Body
	c.BindJSON(&body)

	var oError bool = false
	var oErrors []string
	valid := validation.Validation{}
	valid.Required(body.Type, "Type").Message("Type cannot be empty")
	valid.Min(body.UserID, 1, "UserID").Message("User id is required")

	switch body.Type {
	case "polygon":
		valid.Required(body.Loc, "Loc").Message("Loc cannot be empty")
		if len(body.Loc) != 1 {
			oError = true
			oErrors = append(oErrors, "Loc field has an invalid format")
		} else {
			if len(body.Loc[0]) < 3 {
				oError = true
				oErrors = append(oErrors, "Loc field needs more than 3 coordinates")
			} else {
				firstLat := body.Loc[0][0][0]
				firstLng := body.Loc[0][0][1]
				lastLat := body.Loc[0][len(body.Loc[0])-1][0]
				lastLng := body.Loc[0][len(body.Loc[0])-1][1]

				if firstLat != lastLat || firstLng != lastLng {
					body.Loc[0] = append(body.Loc[0], []float64{firstLat, firstLng})
				}
			}
		}
	case "circle":
		valid.Required(body.Point, "Point").Message("Point cannot be empty")
		if len(body.Point) != 2 {
			oError = true
			oErrors = append(oErrors, "Point field are 2 elements")
		}
		valid.Min(body.Radius, 1, "Radius").Message("Radius is required")
	case "address":
		valid.Required(body.Address, "Address").Message("Address cannot be empty")
	}

	if !valid.HasErrors() && !oError {
		var geoValue string
		switch body.Type {
		case "polygon":
			geoValue = models.AddGeofence(body.UserID, body.Loc)
		case "circle":
			var r, lat, lon string
			r = strconv.Itoa(body.Radius)
			lat = fmt.Sprintf("%f", body.Point[0])
			lon = fmt.Sprintf("%f", body.Point[1])

			client := http.Client{}

			resp, err := client.Get(fmt.Sprintf("%s/circle/%s/%s/%s", os.Getenv("GEO_URI"), lon, lat, r))
			if err != nil {
				log.Fatalln(err)
			}

			var result map[string][][][]float64
			json.NewDecoder(resp.Body).Decode(&result)
			geoValue = models.AddGeofence(body.UserID, result["coordinates"])
		case "address":
		}

		if geoValue != "" {
			msg := "Geofence updated successfully!!!"
			if geoValue != "updated" {
				key := "geofences:" + strconv.Itoa(body.UserID)
				go cache.SetValue(key, geoValue)
				msg = "Geofence created successfully!!!"
			}
			c.JSON(http.StatusOK, gin.H{
				"success": true,
				"msg":     msg,
			})
		}
	} else {
		var errors []string
		errors = append(errors, oErrors...)
		for _, err := range valid.Errors {
			errors = append(errors, fmt.Sprintf("%s", err.Message))
		}
		c.JSON(http.StatusBadRequest, gin.H{
			"success": false,
			"msg":     errors,
		})
	}
}

// CheckInside Checks if an user is inside own geofence
// @Summary Checks if an user is inside own geofence
// @Description This endpoint will return a boolean value as response if an user is inside own geofence
// @Accept  json
// @Produce  json
// @Param user_id path int true "User ID"
// @Param lat path float64 true "Latitude"
// @Param lng path float64 true "Longitude"
// @Router /geofences/check/:user_id/:lat/:lng [get]
// @Security Bearer
// @Tags Geo
func CheckInside(c *gin.Context) {
	lat := com.StrTo(c.Param("lat")).MustFloat64()
	lng := com.StrTo(c.Param("lng")).MustFloat64()
	ID := com.StrTo(c.Param("user_id")).MustInt()
	re := regexp.MustCompile("[^0]+")

	valid := validation.Validation{}
	valid.Match(lat, re, "lat").Message("Latitude is undefined")
	valid.Match(lng, re, "lng").Message("Longitude is undefined")
	valid.Min(ID, 1, "use_id").Message("user_id must be greater than 1")

	if !valid.HasErrors() {
		inside := models.CheckInside(lat, lng, ID)
		c.JSON(http.StatusOK, gin.H{
			"success":   true,
			"is_inside": inside,
		})
	} else {
		var errors []string
		for _, err := range valid.Errors {
			errors = append(errors, fmt.Sprintf("%s", err.Message))
		}
		c.JSON(http.StatusBadRequest, gin.H{
			"success": false,
			"msg":     errors,
		})
	}
}
