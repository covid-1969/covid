package whitelist

import (
	"net/http"
	"github.com/gin-gonic/gin"
)

var whitelist map[string]bool = map[string]bool{
    "172.17.0.1": false,
	"34.235.150.136": true,
	"200.104.21.125": true,
}

func IPWhiteList() gin.HandlerFunc {
    return func(c *gin.Context) {
		ip := c.ClientIP()
		if !whitelist[ip] {
            c.JSON(http.StatusForbidden, gin.H{

            })

            c.Abort()
            return
        }

        c.Next()

    }
}