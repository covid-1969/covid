package auth

import (
	"net/http"
	"os"
	"strings"

	"github.com/gin-gonic/gin"
	
	"covid/pkg/e"
)

func Check() gin.HandlerFunc {
    return func(c *gin.Context) {
		var code int
		var token, clientID string

		code = e.SUCCESS
		r_token := c.Request.Header["Authorization"]
		
		if len(r_token) < 1 {
			code = e.ERROR_MISSING_TOKEN
		} else {
			token = r_token[0]
			splitToken := strings.Split(token, "Bearer")
			token = strings.TrimSpace(splitToken[1])
			
			clientID = os.Getenv("CLIENT_ID")
			if token != clientID{
				code = e.ERROR_AUTH_TOKEN
			}
		}

		if code != e.SUCCESS {
            c.JSON(http.StatusUnauthorized, gin.H{
                "code" : code,
                "msg" : e.GetMsg(code),
            })

            c.Abort()
            return
        }

        c.Next()

    }
}