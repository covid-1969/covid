FROM golang:1.14-alpine AS builder

WORKDIR $GOPATH/src/covid

COPY go.mod ./
COPY go.sum ./

RUN go get -u github.com/swaggo/swag/cmd/swag

RUN go mod download

COPY . ./
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix nocgo .

FROM alpine 
RUN apk add ca-certificates

ENV MONGODB_URI mongodb://mongoadmin:secret@172.17.0.4:27017/admin?tls=false
ENV PORT 8800
ENV CLIENT_ID Phei0ahV0aiquiedieko7nedei8viela
ENV REDIS_URI 172.17.0.2:6379
ENV GEO_URI http://172.17.0.1:8000
ENV REDIS_PASS 1234
ENV GIN_MODE release

COPY --from=builder /go/src/covid ./

EXPOSE 8800

# CMD ["./covid"]