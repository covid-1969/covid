package main

import (
	"fmt"
	"net/http"

	"covid/pkg/setting"
	"covid/routers"
)

// @title Covid API
// @version 1.0
// @description Covid API built using golang and gin-gonic framework
// @termsOfService http://swagger.io/terms/
// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html
// @BasePath /api/v1
// @securityDefinitions.apikey Bearer
// @in header
// @name Authorization
// @schemes http https
// @tag.name Geo
// @tag.description Endpoints to execute geo operations
func main() {
	router := routers.InitRouter()

	s := &http.Server{
		Addr:           fmt.Sprintf(":%d", setting.HTTPPort),
		Handler:        router,
		ReadTimeout:    setting.ReadTimeout,
		WriteTimeout:   setting.WriteTimeout,
		MaxHeaderBytes: 1 << 20,
	}

	s.ListenAndServe()
}
