package e

var MsgFlags = map[int]string {
    SUCCESS : "ok",
    ERROR : "fail",
    ERROR_MISSING_TOKEN: "Missing token",
    ERROR_AUTH_TOKEN: "Invalid token",
}

func GetMsg(code int) string {
    msg, ok := MsgFlags[code]
    if ok {
        return msg
    }

    return MsgFlags[ERROR]
}