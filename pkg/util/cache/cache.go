package cache

import (
	"fmt"
	"os"

	"github.com/go-redis/redis/v7"
)

var client *redis.Client

func init() {
	client = redis.NewClient(&redis.Options{
		Addr:     fmt.Sprintf("%s", os.Getenv("REDIS_URI")), // use default Addr
		Password: fmt.Sprintf("%s", os.Getenv("REDIS_PASS")),
		DB:       0, // use default DB
	})
}
