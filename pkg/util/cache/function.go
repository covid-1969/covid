package cache

import "fmt"

func CheckConn() (res string) {
	res, err := client.Ping().Result()
	if err != nil {
		fmt.Println(err)
		return ""
	}
	fmt.Println(res)
	return
}

func GetValue(key string) (val string) {
	val, err := client.Get(key).Result()
	if err != nil {
		val = ""
	}
	return
}

func SetValue(key string, val string) {
	_ = client.Set(key, val, 0).Err()
}

func HSetvalue(key string, data interface{}) {
	_ = client.HSet(key, data)
}

func HGetvalue(key string) (result map[string]string) {
	res := client.HGetAll(key)
	result, _ = res.Result()
	return
}

func Keys(pattern string) (keys []string) {
	result := client.Keys(pattern)
	keys, _ = result.Result()
	return
}
